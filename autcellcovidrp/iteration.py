'''
===============================
    Data analysis
===============================
'''

import glob, os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import datetime
from celluloid import Camera
import grid as g

# read data
def read_data(cityname):
    
    j = 0
    with open(cityname, 'r') as f:
        for line in f:
            j += 1
    
    cellsevolution = np.zeros((j,4))
    
    i = 0
    with open(cityname, 'r') as f:
        for line in f:
            parts = line.split(' ')
            cellsevolution[i][0] = int(float(parts[0]))
            cellsevolution[i][1] = int(float(parts[1]))
            cellsevolution[i][2] = int(float(parts[2]))
            cellsevolution[i][3] = int(float(parts[3]))
            
            i = i + 1
            
    return cellsevolution

# timecounter
def read_timecounter(cityname):
   
    timeevolution = []
    i = 0
    with open(cityname, 'r') as f:
        for line in f:
            parts = line.split(' ')
            timeevolution.append(datetime.datetime.strptime(parts[0],'%H:%M:%S.%f'))
            i = i + 1
            
    return timeevolution

def read_city(cityname):

    j = 0
    with open(cityname, 'r') as f:
        for line in f:
            j += 1
    
    cells = np.zeros((j,4))
    i = 0
    with open(cityname, 'r') as f:
        for line in f:
            parts = line.split(' ')
            
            cells[i][0] = i
            cells[i][1] = int(float(parts[0]))
            cells[i][2] = int(float(parts[1]))
            cells[i][3] = int(float(parts[2]))
            i = i + 1

    return cells

''' Open data '''
entries = os.listdir()
#os.chdir('/home/lucas/Downloads/fiscomp5/fiscomp5')


for entry in entries:
    if entry == 'data.txt':
        cellsevo = read_data(entry)
    elif entry == 'timecounter.txt':
        timeevo = read_timecounter(entry)

cellstatus = np.zeros((200,5000,4))
j = 0
'''
fig, axes = plt.subplots(ncols=4, nrows=2)
plt.ion()
camera = Camera(fig)
for entry in sorted(glob.glob('*.txt'))[2:-1]:
    print(entry)
    cellstatus[j] = read_city(entry)

    Grid = g.create_grid(10000000)
    
    suscept, infect, recupered, death = cellsevo.T
    Grid = g.suscept_positionupdate(cellstatus[j], Grid)
    Grid_infection = g.infected_positionupdate(cellstatus[j], Grid)
    Grid_recupered = g.recupered_positionupdate(cellstatus[j], Grid)
    Grid_death = g.dead_positionupdate(cellstatus[j], Grid)

    axes[0][0].imshow(Grid, cmap='Blues')
    axes[0][1].imshow(Grid_infection, cmap='Reds')
    axes[0][2].imshow(Grid_recupered, cmap='Greens')
    axes[0][3].imshow(Grid_death, cmap='Greys')

    axes[1][0].plot(suscept[:i], 'o',color = 'blue' , lw = 0.8)
    axes[1][0].set_xlim([0, i])
    axes[1][1].plot(infect[:i], 'o',color = 'red' , lw = 0.8)
    axes[1][1].set_xlim([0,i])
    axes[1][2].plot(recupered[:i], 'o',color = 'green' , lw = 0.8)
    axes[1][2].set_xlim([0,i])
    axes[1][3].plot(death[:i], 'o',color = 'gray' , lw = 0.8)
    axes[1][3].set_xlim([0,i])

    camera.snap()

animation = camera.animate()
animation.save('test.gif', writer = 'imagemagick')
'''

''' plot data '''
# I'm just plotting points here, but you could just as easily use a bar.
t = np.arange(0,200)
plt.gca().yaxis.set_major_formatter(mdates.DateFormatter('%H:%M:%S'))
plt.plot(t,timeevo)
plt.gcf().autofmt_xdate()
plt.show()


suscept, infect, recupered, death = cellsevo.T

#fig, axes = plt.figure()
plt.plot(suscept, 'o',color = 'blue' , lw = 0.8, label="Susceptíveis")
plt.plot(infect, 'o',color = 'red' , lw = 0.8, label="Infectados")
plt.plot(recupered, 'o',color = 'green' , lw = 0.8, label="Recuperados")
plt.plot(death, 'o',color = 'gray' , lw = 0.8, label="Mortos")
plt.xlabel('Dias')
plt.ylabel('Nº de autômatos')
plt.legend()
plt.show()


