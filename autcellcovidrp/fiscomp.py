'''
===============================
    Cell Functions
===============================
'''

import numpy as np
import random

def cells_initialstatus(cells, Population, Pop_ratio=1):
    Population = Population*Pop_ratio
    for i in range(Population):
        cells[i][2] = 1
    return cells

def cells_initialinfected(cells, Population, Pop_ratio=1):
    Population = Population*Pop_ratio
    randomcell = np.random.randint(Population)

    cells[randomcell][2] = 2

    return cells

def cells_infectionspread(cells, grid):

    xlim = grid.shape[0]-1
    ylim = grid.shape[1]-1

    for i in range(cells.shape[0]):

        if cells[i][2] == 2:
            x, y = cells[i][0], cells[i][1]

            if x != 0 and y != 0 and x != xlim and y != ylim:
                xlist, ylist = [x-1,x,x+1],[y-1,y,y+1]
            else:
                if x == 0 and y == 0:
                    xlist, ylist = [x,x+1], [y,y+1]
                if x == 0 and y == ylim:
                    xlist, ylist = [x,x+1], [y-1,y]
                if x == 0 and y != 0 and y != ylim:
                    xlist, ylist =  [x,x+1], [y-1,y,y+1]
                if x == xlim and y == 0:
                    xlist, ylist = [x-1,x], [y,y+1]
                if x == xlim and y == ylim:
                    xlist, ylist = [x-1,x], [y-1,y]
                if x == xlim and y != 0 and y != ylim:
                    xlist, ylist =  [x-1,x], [y-1,y,y+1]
                if y == 0 and x != 0 and x != ylim:
                    xlist, ylist = [x-1,x,x+1], [y,y+1]
                if y == ylim and x != 0 and x != ylim:
                    xlist, ylist = [x-1,x,x+1], [y-1,y]

            for j in range(len(xlist)):
                for k in range(len(ylist)):
                    for ii in range(cells.shape[0]):
                        if cells[ii][0] == xlist[j] and cells[ii][1] == ylist[k] and cells[ii][2] == 1:
                            infect = np.random.randint(0,100)
                            if infect > 91:
                                cells[ii][2] = 2
    return cells


def cells_randompos(cells, grid):
    ''' A function for creation of a grid'''

    for i in range(cells.shape[0]):
        x = np.random.randint(len(grid[0]))
        y = np.random.randint(len(grid[0]))

        cells[i][0], cells[i][1] = x, y

    return cells

def cells_positeration(cells, grid):
    xlim = grid.shape[0]-1
    ylim = grid.shape[1]-1

    for i in range(cells.shape[0]):
        x, y = cells[i][0], cells[i][1]

        if cells[i][2] == 4:
            pass
        else:

            if x != 0 and y != 0 and x != xlim and y != ylim:
                xinc, yinc = random.randint(-1,1), random.randint(-1,1)
            else:
                if x == 0 and y == 0:
                    xinc, yinc = random.randint(0,1), random.randint(0,1)
                if x == 0 and y == ylim:
                    xinc, yinc = random.randint(0,1), random.randint(-1,0)
                if x == 0 and y != 0 and y != ylim:
                    xinc, yinc =  random.randint(0,1), random.randint(-1,1)
                if x == xlim and y == 0:
                    xinc, yinc = random.randint(-1,0), random.randint(0,1)
                if x == xlim and y == ylim:
                    xinc, yinc = random.randint(-1,0), random.randint(-1,0)
                if x == xlim and y != 0 and y != ylim:
                    xinc, yinc =  random.randint(-1,0), random.randint(-1,1)
                if y == 0 and x != 0 and x != ylim:
                    xinc, yinc = random.randint(-1,1), random.randint(0,1)
                if y == ylim and x != 0 and x != ylim:
                    xinc, yinc = random.randint(-1,1), random.randint(-1,0)

            if cells[i][2] == 2:
                cells[i][3] += 1
                if cells[i][3] == 12:
                    cells[i][2] = cells_finalstage()

            cells[i][0], cells[i][1] = x + xinc, y + yinc

    return cells

def cells_finalstage():
    death_rate = 0.035
    death_rate = death_rate * 1000
    x = random.randint(0,1000)

    if x < death_rate:
        return 4
    else:
        return 3

def create_cellsmatrix(Population, Pop_ratio=1):
    #   Create a Matrix
    #   pos x, pos y, condition

    Population = Population*Pop_ratio

    cells = np.zeros((Population, 4))

    cells = cells_initialstatus(cells, Population, Pop_ratio)

    return cells

'''
=========================================
    Cellular Automata Grid
=========================================
'''

import numpy as np
import matplotlib.pyplot as plt
import os

from tqdm import tqdm
from celluloid import Camera
from datetime import datetime

def create_grid(Area, Area_ratio = 100):
    ''' A function for creation of a grid'''

    # Area ratio
    # 1 km^2 -> 100 (100m^2)

    # number of grid divisions
    gridcounts = Area * Area_ratio

    # grid side
    gridside = int(np.sqrt(gridcounts))

    # create Matrix
    grid = np.zeros((gridside,gridside))

    return grid

def suscept_positionupdate(cells, grid):

    grid = np.zeros(grid.shape)

    for i in range(cells.shape[0]):
        if cells[i][2] == 1:
            x, y = int(cells[i][0]), int(cells[i][1])
            grid[x][y] += 1

    return grid

def infected_positionupdate(cells, grid):

    grid = np.zeros(grid.shape)

    for i in range(cells.shape[0]):
        if cells[i][2] == 2:
            x, y = cells[i][0], cells[i][1]
            grid[int(x)][int(y)] += 1

    return grid

def recupered_positionupdate(cells, grid):

    grid = np.zeros(grid.shape)

    for i in range(cells.shape[0]):
        if cells[i][2] == 3:
            x, y = cells[i][0], cells[i][1]
            grid[int(x)][int(y)] += 1

    return grid
    
def dead_positionupdate(cells, grid):

    grid = np.zeros(grid.shape)

    for i in range(cells.shape[0]):
        if cells[i][2] == 4:
            x, y = cells[i][0], cells[i][1]
            grid[int(x)][int(y)] += 1

    return grid


def count_data(cells, i):


    if i == 0 and os.path.isfile('data.txt') == True:
        os.remove('data.txt')

    w, x, y, z  = 0, 0, 0, 0
    for i in range(cells.shape[0]):
        if cells[i][2] == 1:
            w += 1
        elif cells[i][2] == 2:
            x += 1
        elif cells[i][2] == 3:
            y += 1
        elif cells[i][2] == 4:
            z += 1

    with open('data.txt', 'a') as f:
        f.write('{} {} {} {}\n'.format(w,x,y,z))

    data = np.zeros((i,4))

    j = 0
    with open('data.txt', 'r') as f:
        for line in f:
            parts = line.split(' ')
            data[j][0] = int(float(parts[0]))
            data[j][1] = int(float(parts[1]))
            data[j][2] = int(float(parts[2]))
            data[j][3] = int(float(parts[3]))

            j += 1

    return data

def grid_visualization(Grid):

    plt.matshow(Grid, cmap='Blues')
    plt.show(block=False)
    plt.pause(1)
    plt.close()

def grid_visualization_infection(cells, Grid):

    Grid_infection = infected_positionupdate(cells, Grid)
    plt.matshow(Grid_infection, cmap='Greens')
    plt.show(block=False)
    plt.pause(1)
    plt.close('all')

def gvisualization(cells, Grid, ix):

    fig, axes = plt.subplots(ncols=4, nrows=2)
    plt.ion()
    start_time = datetime.now()
    camera = Camera(fig)
    bar = tqdm(total=ix, position=0)
    for i in range(ix):

        cell = cells_positeration(cells, Grid)
        cell = cells_infectionspread(cells, Grid)

        data = count_data(cells,i)
        suscept, infect, recupered, death = data.T

        Grid = suscept_positionupdate(cells, Grid)
        Grid_infection = infected_positionupdate(cells, Grid)
        Grid_recupered = recupered_positionupdate(cells, Grid)
        Grid_death = dead_positionupdate(cells, Grid)

        axes[0][0].imshow(Grid, cmap='Blues')
        axes[0][1].imshow(Grid_infection, cmap='Reds')
        axes[0][2].imshow(Grid_recupered, cmap='Greens')
        axes[0][3].imshow(Grid_death, cmap='Greys')

        axes[1][0].plot(suscept[:i], 'o',color = 'blue' , lw = 0.8)
        axes[1][0].set_xlim([0, i])
        axes[1][1].plot(infect[:i], 'o',color = 'red' , lw = 0.8)
        axes[1][1].set_xlim([0,i])
        axes[1][2].plot(recupered[:i], 'o',color = 'green' , lw = 0.8)
        axes[1][2].set_xlim([0,i])
        axes[1][3].plot(death[:i], 'o',color = 'gray' , lw = 0.8)
        axes[1][3].set_xlim([0,i])

        end_time = datetime.now()
        with open('timecounter.txt', 'a') as f:
            f.writelines('{} \n'.format(end_time-start_time))
        save_city('rp'+str(i), Grid, cells)
        bar.update()
        camera.snap()
    bar.close()

    animation = camera.animate()
    animation.save('test.gif', writer = 'imagemagick')

'''
=========================================
    Cellular Automata Ctiies
=========================================
'''

import numpy as np

def create_malha():
    return


def create_city(cityname, Population, Area, Pop_ratio=1, Area_ratio=100):
    # first create city_grid and city_cellsmatrix
    grid = create_grid(Area, Area_ratio)
    cellsmatrix = create_cellsmatrix(Population)

    # random position of cells
    cellsmatrix = cells_randompos(cellsmatrix, grid)

    # read the position of cells e update cells matrix
    grid_popdensity = positionupdate(cellsmatrix, grid)

    # grid_visualizer
    grid_visualization(grid_popdensity)

    return grid, cellsmatrix


def save_city(cityname, grid, cellsmatrix):

    with open(cityname + '.txt', 'w') as f:
        for i in range(cellsmatrix.shape[0]):
            f.write('{} {} {} \n'.format(cellsmatrix[i][0],cellsmatrix[i][1],cellsmatrix[i][2]))

    return print( cityname + ' saved data')

def read_city(cityname, Population, Pop_ratio=1):

    cells = np.zeros((Population,3))
    i = 0
    with open(cityname + '.txt', 'r') as f:
        for line in f:
            parts = line.split(' ')
            cells[i][0] = int(float(parts[0]))
            cells[i][1] = int(float(parts[1]))
            cells[i][2] = int(float(parts[2]))

            i = i + 1

    return cells

'''
=========================================
    Cellular Automata Spreading Covid
=========================================
'''


# Teste Def

area = 5
pop = 5000

# Create Grid
grid = create_grid(area)

# Create cell list
cell = create_cellsmatrix(pop)

# Cell initial position
cell = cells_initialstatus(cell, pop)

# Cell random pos
cell = cells_randompos(cell, grid)

# read the position of cells e update cells matrix
grid_popdensity = suscept_positionupdate(cell, grid)

# grid_visualizer
#g.grid_visualization(grid_popdensity)

# include initial infected
cell = cells_initialinfected(cell, pop)

gvisualization(cell, grid, 200)

grid_popdensity = suscept_positionupdate(cell, grid)

grid_visualization(grid_popdensity)

save_city('Batatais', grid_popdensity, cell )

cellsread = read_city('Batatais', pop)
